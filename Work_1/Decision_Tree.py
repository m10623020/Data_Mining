# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import urllib.request
from sklearn.tree import DecisionTreeClassifier as DTC
from sklearn.cross_validation import train_test_split
from sklearn.tree import export_graphviz
from sklearn import metrics
from sklearn.feature_extraction import DictVectorizer

url = 'http://archive.ics.uci.edu/ml/machine-learning-databases/00379/PhishingData.arff'
source = urllib.request.urlopen(url)
source_read = source.read()
save_data = source_read.decode("utf-8")

mypath = '/home/yoman/Data_Mining_Work/'

save_data = save_data.split("\n")

x = 0
while x<14:
    del save_data[0]
    x+=1

x = 0
for a in save_data:
    save_data[x] = save_data[x].split(",")
    x+=1
del save_data[-1]

save_file = pd.DataFrame(save_data , columns=['SFH ' , 'popUpWidnow ' , 'SSLfinal_State' , 'Request_URL' , 'URL_of_Anchor' , 'web_traffic ' ,
                                              'URL_Length ' , 'age_of_domain ' , 'having_IP_Address' , 'Result'])
sort_data = save_file.sort_values('Result', ascending=False)
save_file.to_csv(mypath+'MPDS.csv', sep='\t', encoding='utf-8')

x = save_file.iloc[: , :9].as_matrix().astype(int)
y = save_file.iloc[: , 9].as_matrix().astype(int)

train_x,test_x,train_y,test_y = train_test_split(x,y,test_size = 0.3)

dfx = pd.DataFrame(test_x , columns=['SFH ' , 'popUpWidnow ' , 'SSLfinal_State' , 'Request_URL' , 'URL_of_Anchor' , 'web_traffic ' ,
                                              'URL_Length ' , 'age_of_domain ' , 'having_IP_Address'])
dfy = pd.DataFrame(test_y , columns=['Result'])
df = pd.concat([dfx , dfy] , axis=1)

column_label = ['SFH ' , 'popUpWidnow ' , 'SSLfinal_State' , 'Request_URL' , 'URL_of_Anchor' , 'web_traffic ' , 'URL_Length ' , 'age_of_domain ' , 'having_IP_Address' , 'Result']

writer = pd.ExcelWriter(mypath + 'Test_Out_Data.xlsx')
df.to_excel(writer,'Test_Data')
writer.save()

dtc = DTC(criterion='entropy',max_depth=11,min_samples_split=40,min_impurity_split=0.25)
dtc.fit(train_x , train_y)

vec = DictVectorizer()
dummy_x = vec.fit_transform(column_label) .toarray()
print(str(dummy_x))
print(vec.get_feature_names())

data_predicted = dtc.predict(test_x)

with open("Output_Tree.dot" , 'w') as f:
    f = export_graphviz(dtc, out_file = f)   

accuracy = metrics.accuracy_score(test_y,data_predicted)

    